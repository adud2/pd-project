# PureData ATIAM project

## Context

_Sadi Moma_ is a traditional Bulgarian song, relatively unknown in Bulgaria,
yet very popular among Traditional Dance groups in the United States. During a
"bardic circle", Richard M. Stallman decided to write new lyrics above it (a
so-called _filksong_). He told afterward [the genesis of this song].

> I asked myself, what topic should it be about? I realized I had never written
> a filksong relating to free software, so I figured it was time I did.

I followed quite the same path for this PureData project. I asked myself:
"Compared to Max, what are the advantages of PureData?". A very scarce standard
library, a minimalistic outdated interface (written in Tcl, argh), a terrible
UX, flashy colors… Except from the fact that it uses very low resources, and
thus can run on embedded systems, there is one key feature that makes PureData
far more interesting than Max/MSP: PureData is a free software (BSD-3-Clause).
Having this in mind, doing my own arrangement of the Free Software song using
only free softwares came as obvious.

## Softwares

Among the softwares used :

 * [LilyPond] for scores and Midi generation,
 * [Rosegarden] for Midi sequencing
 * [Vmpk] to play midi with keyboard
 * [PureData] (obviously) for sound generation
 * and many more (git, nano, sed, VoidLinux...)

Having a few problems with Jack (probably sampling problems between Jack and
Pd), I sticked to basic ALSA configuration. This forced me to start PureData
first, and slowed down Rosegarden, which tried to gain access to ALSA.

Almost all of the files of this project are released under a free-software
license (GPLv3), hence the copyright patch at the begining of each patch, apart
from `waves/kick~.pd`, which is taken from Ninon Devis course.

## Installing this project

Samples and recordings are not part of the Git repo. One can download them
using the download script `./samples-dl` (or simply by going on
https://dudermel2.ignorelist.com/s/zAwCgo9HysWN2FY/download unzipping the
archive and putting the archive content at the root of the project)

This project has only been tested on GNU/Linux, no production environment
tested.

## Running the project

There was a former wrapper to load all files at startup, inside `./pd.sh`. This
has been replaced by `declare` boxes. Yet, these boxes don't handle the
`-helppath` option. In order to get help by rightclicking on patches, must
still use `./pd.sh`.

## Hierarchy

    .
    ├── annex               files other than Pd (lilypond, abc, rosegarden)
    ├── attic               unused files
    ├── doc                 help files
    ├── generator           random melodies generator
    ├── help.pd             a collection of all patches used
    ├── LICENSE.txt         the GNU GPL License
    ├── main.pd             the main patch used to record audio
    ├── pd.sh               wrapper for PureData
    ├── samples             audio samples used (to be downloaded with dw.sh)
    ├── tests               experiments around patches
    ├── TODO.org            perspectives and future work
    ├── tries               recordings (to be downloaded with dw.sh)
    ├── utils               utility patches, not linked with this project
    │   ├── list            list utilities
    │   ├── sig             signals utilities
    │   └── step            step sequencer utilities
    └── waves               elements generating sound


## Challenges

Working on _Sadi Moma_ is relatively hard: first it has irregular beats, 7/8 is
decomposed as (3+2+2)/8, and it is in Phrygian Mode (this part has not been
treated, so Phrygian is still equivalent to Major in this model).

Working on PureData without mouse (only trackpad) was also an interesting
challenge, that is why I avoided as much as possible to patch a lot of wires.
Many strategies can be used to avoid clicking a lot:

 * Recursivity (256 sequencer is a pair of 128 sequencers, which are a pair of
 64 sequencers, etc)

 * Clones

 * Packing with list comprehension

 * Using variable (I am not very convinced by this method: variables names are
 well hidden deep inside Pd interface)

## Generator

The generator uses Markov process to predict a note using the previous note and
a target chord. Notes are then sampled randomly, giving piority to the nearest.

The aim of this generator is to be rhythm-agnostic, particularily useful when
dealing with 7/8 time. Yet this is not working perfectly well. It also needs to
know how much time separates two strong beats (typically 3 then 2 then 2 in
7/8). For the moment, this part has been hacked outside of the generator core.

A first implementation of this model in Python encouraged me to implement list
comprehension in Pd, in order to simply translate the code.


[the genesis of this song]: https://www.gnu.org/music/writing-fs-song.html
[LilyPond]: https://lilypond.org
[Rosegarden]: http://rosegardenmusic.com/
[Vmpk]: http://vmpk.sourceforge.net
[PureData]: https://puredata.info
