\version "2.7.40"
\header {
  composer = "Trad."
  footnotes = ""
  origin = "Bulgarian"
  tagline = "Lily was here 2.23.5 -- automatically converted from ABC"
  title = "Sadi Moma"
  transcription = "Joakim Olsson"
}
voiceB =  {
  \set Score.defaultBarType = ""

  d''4    c''8    b'4    a'4  \bar "|"   b'4    c''8    b'8    a'8    g'4  
  \bar "|"   g'4.    a'4.    b'8  \bar "|"   c''4.    b'4    b'8    d''8  
  \bar "|"   a'4.    a'2  \bar "|"   d''4    c''8    b'2  \bar "|"   d''4    c''8 
  b'4    a'4  \bar "|"   b'4    c''8    b'8    a'8    g'4  \bar "|"     g'4.   
  a'4.    b'8  \bar "|"   c''4.    b'4    b'8    d''8  \bar "|"   a'4.    a'2  
  \bar "|"   a'2..  \bar "|."   
}
voiceC =  {
  \set Score.defaultBarType = ""

  b'4    a'8    g'4    d'4  \bar "|"   g'4    g'8    g'4    d'4  \bar "|"   
  g'4.    g'2  \bar "|"   g'4.    g'4    g'4  \bar "|"   d'4.    d'2  \bar "|"   
  g'2..  \bar "|"   b'4    a'8    g'4    d'4  \bar "|"   g'4    g'8    g'4    d'4 
  \bar "|"   g'4.    g'2  \bar "|"     g'4.    g'4    g'4  \bar "|"   d'4.    
  d'2  \bar "|"   e'2..  \bar "|."   
}

voiceD = {
  \set Score.defaultBarType = ""
  \clef "bass"
  \relative g,{
    g4. ~ g4 fis4 d4.~ d4 b4
    b4. ~ b4 d4 e4. d2
    fis4. ~ fis4 d4 g4. ~ g4 g4
  }
}

voicedefault =  {
  \set Score.defaultBarType = ""
  \time 7/4 \key g \major
}

\score{
  \transpose g c'
  <<    
    \context Staff="1"
    {
      \voicedefault
      \voiceB 
    }
    \context Staff="2"
    {
      \voicedefault
      \voiceC 
    }
    \context Staff="3"
    {
      \voicedefault
      \voiceD
    }
  >>
  \layout {
  }
  \midi {}
}
