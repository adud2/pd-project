"""A simple melody generator based on a grid"""

from random import random, randrange

ITNAMES = ["do", "dod", "re", "mib", "mi", "fa", "fad", "sol",
           "sold", "la", "sib", "si"]


# gave up on structured silences: too many exceptions to handle,
# would have been very painful in Pd

# new approach: just don't play some notes with a low probability


def tolily(mel):
    """convert a melody in midi pitchs in human-readable format
    mel: the melody list of pairs (pitch/scale)"""
    fp, fr = mel[0]
    out = [NAMES[fp % 12] + str(fr)]
    for (pp, pr), (ap, ar) in zip(mel, mel[1:]):
        if ap - pp > 6:
            oc = "'"
        elif ap - pp < -5:
            oc = ","
        else:
            oc = ""

        ry = "" if ar == pr else str(ar)
        out.append(ITNAMES[ap % 12] + oc + ry)
    return " ".join(out)


def naive(n):
    """generate very naively a random melody"""
    return [(randrange(45, 74), 2 ** randrange(4)) for _ in range(n)]


def distances(p, chord):
    """compute the distances between a pitch and the pitches of a chord
    p: the given pitch [mod 12]
    chord: the target chord (sorted list of pitchs [mod 12])

    Returns: list of distances to chord notes up to 1 octave excluded
        sorted between -11 and 11"""
    p %= 12
    dists = [pp - p for pp in chord]  # between -11 and 11
    # symmetrize
    rup = [d + 12 for d in dists if d < 0]
    rdo = [d - 12 for d in dists if d > 0]
    return rdo + dists + rup


def normalize(li):
    """scale a list of numbers so that it sums to 1"""
    s = sum(li)
    return [e / s for e in li]


def getDistr(deltas, exp=-1):
    """define Markov Decision Process from pitch to pitch given a chord
    p: the given pitch [mod 12]
    deltas: the list of distances (sorted list in [|-11, 12|[ )
    exp: the repulsion exponent (the smaller the exp the nearer the pitchs are)
    """
    # the main idea is to prefer nearer pitchs
    # with 2 exeptions:
    #    - one should avoid to repeat the same pitch
    #    - after a silence, you can start back everywhere you want
    #      (silences not implemented)
    return normalize([(abs(d))**(exp)
                      if d != 0 else 4**exp
                      for d in deltas])


def sampleDistr(elts, distr):
    """sample from a given discrete distribution
    elts: the elements
    distr: their distribution"""
    # compute cumulative sum (i.e. proba function)
    cum = [0] * len(distr)
    for i in range(1, len(distr)):
        cum[i] = cum[i-1] + distr[i-1]
    uni = random()
    i = 0
    while i < len(cum) and uni > cum[i]:
        i += 1
    return elts[i-1]


# I somehow managed to make a racist IA: until now there was no stranger tone
# in the whole generator
# Hopefully this never happens in Real-Life IAs.


# Known bug: does not handle melodic vs harmonic vs natural minor
# should add direction to melodies

def passage(p, q, tonality):
    """returns possible tones between two tones filling in a tonality
    p: incoming pitch
    q: outgoing pitch
    tonality: a 12 boolean list of allowed and forbidden pitches

    Returns: a list of pitches strictly between p and q (might be empty)
    """
    p, q = min(p, q), max(p, q)
    return [c for c in range(p+1, q) if tonality[c % 12]]


def sampleMelody(p, grid, tona, exp=-1, ppsg=.8):
    """sample a melody starting at given pitch on a grid
    p: start pitch (not mod 12)
    grid: list of chords (sorted list of pitchs [mod 12])"""

    mel = []

    for ch in grid:
        # draw next pitch at random
        deltas = distances(p % 12, ch)
        distr = getDistr(deltas, exp)
        dp = sampleDistr(deltas, distr)
        np = p + dp  # next pitch

        # see if we can insert some note de passage
        psg = passage(p, np, tona)
        if len(psg) == 1 and random() < ppsg:
            mel.append((p, 8))
            mel.append((psg[0], 8))
        else:
            mel.append((p, 4))
        p = np

    mel.append((np, 4))
    return mel


tona = [True, False, True, True, False, True,
        False, True, False, True, False, True]

ex = [[0, 3, 7],
      [2, 7, 10],
      [0, 5, 8],
      [2, 7, 11]]

flat = [it for sub in zip(ex, ex) for it in sub]
