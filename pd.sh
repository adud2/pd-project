#! /bin/sh

ulimit -v 800000 # to avoid memory explosion when filling an infinite list


# not useful anymore, replaced by declare statement
#PARG=$(sed 's/.\+/-path &/' <<- EOF | xargs
#	generator
#	utils
#	utils/sig
#	utils/list
#	utils/step
#	waves
#EOF
#)

# no declare box possible for helppath, so we need to add it here

pd -helppath patches/doc $@
